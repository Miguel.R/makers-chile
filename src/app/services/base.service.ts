import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { from } from 'rxjs'
import 'rxjs/add/operator/timeout';
import {environment as ENV } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class BaseService {

  constructor(
    public http: HttpClient
  ) { }

  public get(url: string, body?: any) {
    var token = localStorage['accessToken'];
    return this.http.get(ENV.API + url, {
      params: body,
      headers: new HttpHeaders().append('Authorization', "Bearer " + token),
    }).timeout(ENV.HTTP.TIME_OUT)
  }

  public getWithoutToken(url: string, body?: any) {
    return this.http.get(ENV.API + url, {
      params: body,
      headers: new HttpHeaders().append('Authorization', "Bearer ")
    }).timeout(ENV.HTTP.TIME_OUT)
  }

  public loginPost(url: string, body?: any) {
    let token = { access_token: ENV.JWT_SECRET }
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
    }); // JSON.stringify(body)
    return this.http.post(ENV.API + url, body, {headers: headers}).timeout(ENV.HTTP.TIME_OUT);
  }

  public postWithoutToken(url: string, body?: any) {
    return this.http.post(ENV.API + url, body, {
    }).timeout(ENV.HTTP.TIME_OUT);
  }

  getAuthData() {
    if ( localStorage.getItem('accessToken') !== null )
      return localStorage.getItem('accessToken')
  }

  /* public post(url: string, body?: any) {
    const token = localStorage['accessToken'];

    return this.http.post(ENV.API + url, {
      params: body,
      headers: new HttpHeaders().append('Authorization', "Bearer " + token)
    }).timeout(ENV.HTTP.TIME_OUT);
  } */

  public post(url: string, body?: any) {
    const token = localStorage['accessToken'];
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json',
    });
    return this.http.post(ENV.API + url, body, {headers: headers}).timeout(ENV.HTTP.TIME_OUT);
  }
}
