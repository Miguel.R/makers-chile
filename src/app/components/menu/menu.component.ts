import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { CustomAlertService } from 'src/app/services/utils/custom-alert.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  public model = {
    left: true,
    middle: false,
    right: false
  };

  public currUrl: string = ''
  constructor(
    public auth: AuthService,
    public router: Router,
    public swal: CustomAlertService
  ) {
    this.currUrl = router.url
  }

  logout() {
    this.auth.logout()
    this.swal.toast('Has cerrado tu sesión exitosamente')
    this.router.navigateByUrl('/login')
  }

  ngOnInit(): void {
  }

}
