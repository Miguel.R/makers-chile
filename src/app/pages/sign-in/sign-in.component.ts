import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import { MustMatch } from '../../_helpers/must-match.validator';
import { Router } from '@angular/router';
import { CustomAlertService } from 'src/app/services/utils/custom-alert.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
  public roles: Array<object> = [
    // ['donor','applicant','sponsor','admin']
    { text: 'Maker', value: 'Maker' },
    { text: 'Solicitante', value: 'Solicitante' },
    { text: 'Patrocinador', value: 'Patrocinador' }
  ]
  registerForm: FormGroup;
  submitted = false;

  constructor(
    private auth: AuthService,
    private formBuilder: FormBuilder,
    private router: Router,
    private swal: CustomAlertService
  ) { }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(6)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required],
      institution: ['', ],
      role: ['Solicitante', Validators.required],
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;
    console.log(this.registerForm.valid)

    if (this.registerForm.invalid) {
        return;
    }

    let userData = this.registerForm.value
    delete userData?.confirmPassword
    this.newUser(userData)
  }

  newUser(user) {
    this.auth.signIn(user).subscribe(
      response => {
        if ( response ) {
          this.swal.toast('La cuenta ha sido creada exitosamente.', 'success')
          this.router.navigateByUrl('/login')
        }
      }, err => {
        this.swal.errorMsg(err)
      }
    )
  }

}
