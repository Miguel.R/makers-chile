import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { CustomAlertService } from 'src/app/services/utils/custom-alert.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup
  submitted = false

  constructor(
    private auth: AuthService,
    private formBuilder: FormBuilder,
    private router: Router,
    private swal: CustomAlertService
  ) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  get f() { return this.loginForm.controls; } 

  onSubmit() {
    let userData = this.loginForm.value
    this.auth.login(userData).subscribe(response => {
      if ( response )
        this.router.navigateByUrl('/dashboard')
        this.swal.toast('Has iniciado sesión exitosamente')
    }, err => {
      console.log(err)
      this.swal.errorMsg(err.error.message)
    })
  }

}