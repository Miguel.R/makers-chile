import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { from, Subject } from 'rxjs'
import 'rxjs/add/operator/timeout';
import {environment as ENV } from '../../environments/environment'
import { BaseService } from './base.service';
import { ErrorsService } from './errors/errors.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  constructor(
    public http: HttpClient,
    public api: BaseService,
    private errorService: ErrorsService
    ) { }

    getAuthData() {
      if ( localStorage.getItem('accessToken') !== null )
        return localStorage.getItem('accessToken')
    }

    login(user) {
      let subject = new Subject<any>()
      this.api.loginPost('/user/login', user).subscribe(
        (res: any) => {
          localStorage.setItem('accessToken', res.accessToken)
          localStorage.setItem('refreshToken', res.refreshToken)
          localStorage.setItem('user_id', res._id)
          localStorage.setItem('role', res.role)
          subject.next(res)
        }, error => {
          subject.error(error)
        }
      )
      return subject
    }

    signIn(data) {
      let subject = new Subject<any>()

      this.api.postWithoutToken('/user/register', data).subscribe(
        data => {
          subject.next(data)
        }, error => {
          subject.error(error.error)
          this.errorService.errorHandler(error) 
        }
      )
      return subject
    }

    logout() {
      // Call logout service...
      localStorage.clear()
    }

    
  }
