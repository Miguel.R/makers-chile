import { Component, OnInit } from '@angular/core';
import { MenuComponent } from '../components/menu/menu.component'

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  public model = {
    left: true,
    middle: false,
    right: false
  };
  constructor() { }

  ngOnInit(): void {
  }

}