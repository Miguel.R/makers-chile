import { Injectable } from '@angular/core';
import Swal from 'sweetalert2'

@Injectable({
  providedIn: 'root'
})
export class CustomAlertService {

  constructor() { }

  public errorMsg(msg) {
    Swal.fire({
      title: 'Error!',
      text: msg,
      icon: 'error',
      // confirmButtonText: 'Cool'
    })
  }

  public toast(msg, icon?) {
    icon ? icon : 'success'
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true,
      onOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })
    
    Toast.fire({
      icon: icon,
      title: msg
    })
  }
}
