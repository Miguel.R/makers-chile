import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { RequestService } from 'src/app/services/request/request.service';
import { CustomAlertService } from 'src/app/services/utils/custom-alert.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(
    private requestService: RequestService,
    private swal: CustomAlertService
  ) {
  }

  public dataUno = [
    { data: [16, 42, 21, 75, 77, 75], label: 'Viseras' },
    { data: [42, 72, 21, 11, 3, 16], label: 'Mascarillas' }
  ]
  dataDos = [
    { data: [231, 160, 121, 75, 190, 54], label: 'filamentos' },
  ]
  public labelUno = ['24/03', '25/03', '26/03', '27/03', '28/03', '29/03']
  public labelDos = ['24/03', '25/03', '26/03', '27/03', '28/03', '29/03']

  public misEstadisticas = {
    label: ['Viseras impresas', 'Viseras entregadas', 'Viseras faltantes'],
    data: [30, 50, 20]
  }

  public requests: any
  public amount: any = []
  public note: any = []


  getRequests() {
    this.requestService.getAll().subscribe(
      res => {
        this.requests = res
        this.requests.map( 
          req => {
            req.total = 0
            req.acceptedRequest.map(
              acceptedRequest => req.total += acceptedRequest.amount
            )
          }
        )
        console.log(this.requests)
      }
    )
  }

  ngOnInit(): void {
    this.getRequests()
  }

  onSubmit(request, i) {
    const newAcceptedRequest = { amount: this.amount[i], note: this.note[i], _id: request._id }

    if ( newAcceptedRequest.amount <= 0 )
      return this.swal.errorMsg('La cantidad debe ser mayor a 0')

    if ( newAcceptedRequest.amount > (request.amount - request.total) )
      return this.swal.errorMsg('La cantidad comprometida no puede ser mayor a la cantidad faltante')

    console.log(newAcceptedRequest)
    this.requestService.postNewAcceptedRequest(newAcceptedRequest).subscribe(
      res => {
        this.swal.toast('Te has comprometido exitosamente con '+ newAcceptedRequest.amount + ' ' +request.category, 'success')
        this.getRequests()
      },
      err => this.swal.toast('Error en los servicios', 'error')
    )
  }
}
