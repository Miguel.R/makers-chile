import { Injectable } from '@angular/core';
import { CustomAlertService } from '../utils/custom-alert.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ErrorsService {

  constructor(
    private swal: CustomAlertService,
    private router: Router
  ) { }

  public errorHandler(err) {
    console.log(err)
    if ( err.status === 401 || err.status === 403 ) {
      this.router.navigateByUrl('/login')
      this.swal.toast('La sesión ha caducado', 'error')
    }
  }
}
