import { Component, OnInit } from '@angular/core';
import { RequestService } from 'src/app/services/request/request.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomAlertService } from 'src/app/services/utils/custom-alert.service';

@Component({
  selector: 'app-donations-form',
  templateUrl: './donations-form.component.html',
  styleUrls: ['./donations-form.component.scss']
})
export class DonationsFormComponent implements OnInit {
  public tipo: string = "Solicitud"
  public categoria: string = "Mascarillas"
  public role: string = 'Maker'
  requestForm: FormGroup
  
  constructor(
    private requestService: RequestService,
    private formBuilder: FormBuilder,
    private swal: CustomAlertService
  ) { 
    this.requestForm = this.formBuilder.group({
      type: ['Solicitud', [Validators.required]],
      category: ['Mascarillas', [Validators.required]],
      detail: ['', [Validators.required]],
      amount: [, [Validators.required, Validators.min(0)]],
    });
  }

  ngOnInit(): void {
    const request = {
      "type": "Solicitud",
      "category": "Filamentos",
      "detail": "Re100 salidas del horno",
      "amount": 12
    }
    this.role = localStorage.getItem('role')
    if ( this.role == 'Maker' ) {
      this.requestForm.value.type = 'Solicitud'
      this.requestForm.value.category = 'Filamentos'
    }

  }

  onSubmit() {
    const requestData = this.requestForm.value
    this.requestService.postNewRequest(requestData).subscribe(
      res => {
        console.log(res)
        this.swal.toast('La solicitud ha sido ingresada exitosamente', 'success')
      }, err => {
        this.swal.errorMsg(err.error.message)
      }
    )
  }

}
