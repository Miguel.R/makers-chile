import { Component } from '@angular/core';
import { AuthService } from './services/auth.service';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { CustomAlertService } from './services/utils/custom-alert.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Makers chile';
  currUrl = ''
  constructor(
    public auth: AuthService,
    private location: Location,
    private router: Router,
    private swal: CustomAlertService
  ) {

    console.log(this.location.path())
    // Sin sesión
    const noAuthRequired = ['/login', '/sign-in']
    if ( !noAuthRequired.includes(this.location.path()) ) {
      // Sin sesión
      if ( !this.auth.getAuthData() ) {
        this.swal.toast('No estás conectado', 'error')
        this.router.navigateByUrl('/login')
      }
    }
  }
  
  ngOnInit() {

  }
}
