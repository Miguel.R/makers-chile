import { Injectable } from '@angular/core';
import { BaseService } from '../base.service';
import { ErrorsService } from '../errors/errors.service';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  constructor(
    private api: BaseService,
    private errorService: ErrorsService
  ) { }

  getAll() {
    const subject = new Subject<any>()
    this.api.get('/request/makersQueue').subscribe(
      res => subject.next(res),
      err => { 
        subject.error(err)
        this.errorService.errorHandler(err) 
      }
    )
    return subject
  }

  postNewRequest(request) {
    const subject = new Subject<any>()
    this.api.post('/request/new', request).subscribe(
      res => subject.next(res),
      err => { 
        subject.error(err)
        this.errorService.errorHandler(err) 
      }
    )
    return subject
  }

  postNewAcceptedRequest(request) {
    const subject = new Subject<any>()
    this.api.post('/request/acceptedRequest', request).subscribe(
      res => subject.next(res),
      err => { 
        subject.error(err)
        this.errorService.errorHandler(err) 
      }
    )
    return subject
  }
}
